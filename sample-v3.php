<?php

error_reporting(E_ALL);                 // moan about any/all problems
ini_set('track_errors', TRUE);          // so $php_errormsg can be used
ini_set('html_errors', FALSE);	        // no HTML markup in error messages

// Tell PHP where to find the library classes. Adjust as required.
include_once '/site/admindir/lib/ucam_rssnews.php';

// The directory in which data from the RSS feed will be stored. Adjust as
// required.
$directory = '/site/admindir/rss-data';

// The location of the RSS feed icon image.
$rssicon = '/images/icon-rss.gif';

// Retrieve and format data for the RSS feed identified by tag 
// 'ucsnews.csx-all', 

$tag = 'ucsnews.csx-all';
$rss = new Ucam_RSSformat();

$result1 = $rss->format_rssdata(
    array(
	'directory' => $directory,
	'tag' => $tag,
	'type' => 'v3',
	'heading' => 'University Computing Service news',
	'headingurl' => 'http://ucsnews.csx.cam.ac.uk/',
	'rsslogourl' => $rssicon,
	'nonewstext' => 'This optional text is shown when there is no news to display',
	'seealsourl' => TRUE,
	'seealsodesc' => 'all Computing Service news',
	)
    );

// In a "real" web page, you'd need to handle errors differently - e.g. simply
// omit the news box from the page. You should not report the technical
// details of errors in web pages, except for convenience during private
// testing, since they'll be meaningless to users and may reveal information
// that could be of value to an attacker.

if ($result1 === FALSE)
{
    $result1 = "<p>sample news-box for the " . htmlentities($tag) . 
	" feed failed: " . $rss->last_error() . "</p>\n";
}
else
{
    $result1 = "<p><b>Using the " . htmlentities($tag) . 
	" news feed:</b></p>\n$result1\n";
}


// Retrieve and format data for the RSS feed identified by tag 
// 'www.admin-all'. 

$tag = 'www.admin-all';
$result2 = $rss->format_rssdata(
    array(
	'directory' => $directory,
	'tag' => $tag,
	'type' => 'v3+image+text',
	'heading' => 'University of Cambridge news',
	'headingurl' => 'http://www.admin.cam.ac.uk/news/',
// RSS feed logo+link is suppressed if no URL supplied
	'rsslogourl' => $rssicon,
	'seealsourl' => 'http://www.admin.cam.ac.uk/news/',
	'maxitems' => 3,
	    )
    );

// In a "real" web page, you'd need to handle errors differently - e.g. simply
// omit the news box from the page. You should not report the technical
// details of errors in web pages, except for convenience during private
// testing, since they'll be meaningless to users and may reveal information
// that could be of value to an attacker.

if ($result2 === FALSE)
{
    $result2 = "<p>sample news-box for the " . htmlentities($tag) . 
	" feed failed: " . $rss->last_error() . "</p>\n";
}
else
{
    $result2 = "<p><b>Using the " . htmlentities($tag) . 
	" news feed:</b></p>\n$result2\n";
}

// Retrieve and format data for the RSS feed identified by tag 
// 'ucsnews.csx-lookup', chosen because it's very often "empty" (no items) and
// would then show the nonewstext.

$tag = 'ucsnews.csx-lookup';
$rss = new Ucam_RSSformat();

$result3 = $rss->format_rssdata(
    array(
	'directory' => $directory,
	'tag' => $tag,
	'type' => 'v3',
	'heading' => 'lookup news',
	'headingurl' => 'http://ucsnews.csx.cam.ac.uk/',
	'rsslogourl' => $rssicon,
	'nonewstext' => 'This optional text is shown when there is no news to display',
	'seealsourl' => TRUE,
	'seealsodesc' => 'all Computing Service news',
	)
    );

// In a "real" web page, you'd need to handle errors differently - e.g. simply
// omit the news box from the page. You should not report the technical
// details of errors in web pages, except for convenience during private
// testing, since they'll be meaningless to users and may reveal information
// that could be of value to an attacker.

if ($result3 === FALSE)
{
    $result3 = "<p>sample news-box for the " . htmlentities($tag) .  
	" feed failed: " . $rss->last_error() . "</p>\n";
}
else
{
    $result3 = "<p><b>Using the " . htmlentities($tag) . 
	" news feed:</b></p>\n$result3\n";
}

?>

<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- For use in real web pages, the CSS definitions should probably be in
    a separate file, referenced here by URL. For this example, the definitions
    are here for simplicity and clarity. 
-->
<title>RSS feed demo</title>
<style type="text/css">

div.news_box {
        background: #f1f5ff;
}
/* float:left in this definition seems to lose the blue background from the entire newsbox */

dl.news_box {
        clear: both;
/*      float: left; */
        width: 100%;
        border-bottom: 1px solid #CCC;
        padding-bottom: 0.5em;
}

dl.news_box dt {
        padding-top: 0.5em;
        padding-bottom: 0;
        padding-left: 21px;
        margin-left: 2px;
        margin-bottom: 0.5em;
        color:#036;
        font-size: 1.2em;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
}

/* float:left in This definition seems to lose newsbox background colour from the stories but not the heading */

dl.news_box dd  {
/*      float: left; */
        padding-left: 21px;
        font-size: 0.95em;
        margin: 0;
}

dl.news_box img {
        background: white;
        padding: 5px;
        margin: 5px 0 5px 5px;
        display: inline;
}

dl.news_box dt img {
        float: left;
        border: none;
        padding: 0 10px 0 0;
        background: none;
        position: relative;
        top: 1px;
        margin: 0;
}

dl.news_box dd img {
        float: right;
        width: 72px;
        height: 72px;
        background:#fff;
        margin-right:5px;
        border:solid 1px #ddd;
}

dl.news_box dt a {
        text-decoration: none;
        color:#036;
}

dl.news_box dt a:hover {
        text-decoration: underline;
        color:#036;
}

dl.news_box dd div.news_item {
        float: left;
        background:#f9f9f9;
        border-bottom:1px #eee solid;
        margin-bottom:5px;
        width:100%;
}

dl.news_box dd div.news_item a:hover {
        color:#c60;
}

dl.news_box dd div.news_item a:visited {
        color:#900;
}

dl.news_box dd p {
        font-size: 1em;
        padding-top: 0.2em;
        padding-left: 5px;
        margin-bottom: 0.4em;
}

dl.news_box dd p.news_item_date {
        font-size:.8em;
        font-style:normal;
        color:#c60;
        display:block;
        border-top:1px #ddd solid;
        border-bottom:0px #eee solid;
        margin:0;
        margin-bottom:-5px;
        padding:0 0 0 5px;
}

dl.news_box dd a {
        font-weight:bold;
        text-decoration: none;
}

dl.news_box dd a:hover {
        text-decoration: underline;
}

dl.news_box dd p.news_item_more {
        margin:0 0 5px 0;
        padding:0;
        text-align:right;
}

dl.news_box dd p.news_item_more a {
        padding-right:23px;
        background:url(/images/icon-arrow-nav-primary.gif) no-repeat 90% 60%;
}

dl.news_box dd p.more a:hover {
}

dl.news_box dd a.news_item_right_link {
    	padding-right:15px;
    	background:url(/images/icon-arrow-nav-primary.gif) no-repeat 100% 60%;
}

dl.news_box div.news_seealso {
    	text-align: right;
}

</style>
</head>

<body>
<?php echo $result1 ?>
<div style="clear: both;" />
<?php echo $result2 ?>
<div style="clear: both;" />
<?php echo $result3 ?>
<!-- Table used here only for simplicity and clarity. In general, CSS should be
     used for page layout, rather than tables. -->
<div style="clear: both;" />
<hr width="100%" />
<table>
<tr><td width="48%" valign="top"><?php echo $result1 ?></td><td width="4%">
 </td><td width="48%" valign="top"><?php echo $result2 ?></td></tr>
</table>
</body>
</html>

