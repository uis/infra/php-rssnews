<?php

  // This file defines two PHP classes, providing facilities for fetching data
  // from RSS news feeds and formatting that information into HTML "news
  // boxes". It is specifically targetted at handling RSS version 2.0 feeds
  // from ucsnews.csx.cam.ac.uk and www.admin.cam.ac.uk, and may need
  // modification for use with feeds from other sources. See the accompanying
  // README file for documentation.
  //
  // Copyright (C) 2008, University of Cambridge
  //
  // This library is free software; you can redistribute it and/or
  // modify it under the terms of the GNU Lesser General Public
  // License as published by the Free Software Foundation; either
  // version 2.1 of the License, or (at your option) any later version.
  //
  // This library is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  // Lesser General Public License for more details.
  //
  // You should have received a copy of the GNU Lesser General Public
  // License along with this library; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
  // MA 02110-1301  USA
  //
  // Although no formal support is offered for this software, enquiries
  // (including reports of errors etc.) can be sent to
  // web-support@ucs.cam.ac.uk .
  //  
  // Modification history:
  //   30 Jun 2008, J.M.Line: initial version (0.1).



  // Enable "track_errors" so that $php_errormsg can be used to report the
  // specific explanation for relevant errors.

  // Ideally, also turn on reporting for all errors, since "trivia" such as
  // using undefined variables may be due to a mistake and cause far more
  // obscure problems later in the class. However, that may need to be
  // commented out if harmless (with luck!) errors elsewhere result in floods
  // of error messages.


ini_set('track_errors', TRUE);		// required setting
error_reporting(E_ALL);			// preferred setting

class Ucam_RSSfetch {

   // IMPORTANT: This class requires that the cURL and XML/SimpleXML PHP
   // extensions are installed, enabled and work correctly.

    // This class is intended primarily for fetching data from the RSS feeds
    // provided by ucsnews.csx.cam.ac.uk and www.admin.cam.ac.uk, which are
    // RSS 2.0 feeds using plain text for the item content (not HTML), and
    // with UTF-8 character encoding. Support is included for an extension to
    // the RSS (done in an "official" way, using XML namespaces) that is used
    // to associate image files with each item in the www.admin feeds.
    //
    // It is intended for use from scripts run by cron jobs (Unix/Linux) or 
    // equivalent (normally), or at the shell command prompt (for testing),
    // NOT from web pages. Consequently, errors messages are written to 
    // STDERR so they should be seen either in terminal output or mail from
    // cron.
    //
    // While this class may work with feeds from other web sites,
    // modifications might be needed if those feeds used the RSS XML elements
    // in a significantly different way.
    //
    // Security considerations (e.g. to avoid the risk of cross-site scripting
    // (XSS) attacks against client software viewing the RSS-derived data)
    // require that care must taken to correctly handle whatever character
    // encoding is used for the RSS data and apply character-entity encoding
    // where appropriate.
    //
    // For feeds that include HTML markup (which, in RSS 2.0, is not
    // distinguished in any way) it would be distinctly tricky to ensure that
    // the embedded HTML markup neither wrecked the page layout of web pages
    // incorporating the data saved by this class, nor resulted in XSS or
    // other security issues for people viewing the pages. Making the required
    // changes to handle such feeds securely is the responsibility of anyone
    // who chooses to use this class (or derivatives of it) and feels the need
    // to take that risk.


    // warn($text) - write warning message ($text) to stderr - with newline
    // added. Must be called as a static class function. not as an object
    // method.

    protected function warn($text)
    {
	$backtrace = debug_backtrace();	// to get name of calling function
	// The first element ([0]) of $backtrace should be details of the 
	// current function - boring, but the next element ([1]) should be
	// our caller, including the function name - though we should check
	// the array elements exist "just in case".

	if (array_key_exists(1, $backtrace) and 
	    array_key_exists('function', $backtrace[1]))
	{
	    $caller = $backtrace[1]['function']; // our caller's name
	}
	if ($caller != '') $caller .= ': ';

	fwrite(STDERR, __FILE__ . ": $caller" . $text . "\n");
	return;
    }

    // quit($text) - write warning message ($text) to stderr - with newline
    // added, then exit with error status (return code non-zero). Must be
    // called as a static class function. not as an object method.

    protected function quit($text)
    {
	$backtrace = debug_backtrace();	// to get name of calling function
	// The first element ([0]) of $backtrace should be details of the 
	// current function - boring, but the next element ([1]) should be
	// our caller, including the function name - though we should check
	// the array elements exist "just in case".

	if (array_key_exists(1, $backtrace) and 
	    array_key_exists('function', $backtrace[1]))
	{
	    $caller = $backtrace[1]['function']; // our caller's name
	}
	if ($caller != '') $caller .= ': ';

	fwrite(STDERR, __FILE__ . ": $caller" . $text . "\n");
	exit(1);
    }


    // rename_file($from, $to) - attempts to rename() $from as $to, with
    // recovery action if renaming fails on Windows due to the target already
    // existing.
    //
    // If the initial rename fails but the target does not yet exist, it's
    // not the "Windows - already exists" problem, so just report failure.
    //
    // If the initial rename fails and the target exists, try unlink()-ing it,
    // and if that succeeds, try the rename again. If either of those fails,
    // report the error.
    //
    // NB Race conditions could result in the file existence test not matching
    // the state at the time the rename failed, but that's the best we can
    // do... Note that renaming or unlinking may still fail if the file's in
    // use by a web page reading the data...

    protected function rename_file($from, $to)
    {
	if (! @rename($from, $to))
	{
	    if (! file_exists($to))
	    {
		self::quit("renaming $from as $to failed: $php_errormsg");
	    }

	    if (! @unlink($to))
	    {
		self::quit("error deleting $to (before renaming $from, " .
			   "after initial rename failed): $php_errormsg");
		
	    }
	    if (! @rename($from, $to))
	    { 
		self::quit("error renaming $from as $to (after deleting " .
			   "old $to): $php_errormsg");    

	    }
	}
    }

    // save_data($info, $new_timestamp, $outfile, $toutfile, $tsfile,
    // $ttsfile) - save the relevant data (in $info) from the RSS feed to
    // $toutfile with its last modification timestamp (or 0 if not available)
    // saved in $tsfile, writing those files via intermediate files ($toutfile
    // and $ttsfile) and then renaming, to avoid incomplete data being read
    // (while the file is being written or if writing the files fails part-way
    // through).
    //
    // That should also minimise the risk of muddles if delays (e.g. RSS feed
    // site unresponsive) results in multiple runs of scripts using get_feed
    // ending up overlapping - though completely avoiding that in a
    // system-independent way would be tricky and/or clumsy.

    protected function save_data($info, $new_timestamp, $outfile, $toutfile, 
				 $tsfile, $ttsfile)
    {

	// We have the output file content in $info. Now attempt to create the
	// data file, initially under a temporary name and renaming it only
	// if written successfully.
	
	$out = @fopen($toutfile, 'w');
	if (! $out)
	{
	    self::quit("opening $toutfile for writing failed: $php_errormsg");
	}

	if (! @fwrite($out, $info))
	{
	    $msg = "writing to $toutfile failed: $php_errormsg"; // save reason
	    @fclose($out);		// may change $php_errormsg
	    self::quit($msg);
	}

	if (! @fclose($out)) 
	{ 
	    self::quit("closing '$toutfile' after writing failed: " . 
		       "$php_errormsg"); 
	}
	
	// Fetching the data and writing the intermediate output file
	// succeeded.  Create a corresponding intermediate timestamp file
	// containing the modification timestamp of the XML data and rename
	// both to their final names. Intermediate files + renaming are used
	// to minimise the risk that the files will be read when only
	// half-written or in a mutally-inconsistent state. The data file is
	// renamed first. That way, if something prevents renaming the
	// timestamp file, a later feed-fetching run will simply re-fetch the
	// data needlessly - rather than potentially see a timestamp for a
	// newer version of the data than is actually in the data file.
	//
	// If we didn't receive a timestamp, simply write the default 0 value
	// to the file - that's older than any plausible value (since
	// the date it represents is 20+ years before the web was invented :-),
	// it should never sabotage a get-if-modified request.

	// This may not be sufficient to cope with overlapping script runs
	// (though it should minimise any problems), e.g.  from successive,
	// very frequent cron jobs that are all delayed by very slow response
	// from the RSS feed source and then run at essentially the same time
	// (when the RSS feed "wakes up"). Unclear how best to tackle that (in
	// a clean, portable and reliable way).

	$out = @fopen($ttsfile, 'w');
	if (! $out)
	{
	    self::quit("opening $ttsfile for writing failed: $php_errormsg");
	}
	if (! @fwrite($out, "$new_timestamp\n"))
	{
	    $msg = "writing to $ttsfile failed: $php_errormsg";	// save reason
	    @fclose($out);		// may change $php_errormsg
	    self::quit($msg);
	}
	if (! @fclose($out)) 
	{ 
	    self::quit("closing '$ttsfile' after writing failed: $php_errormsg"); 
	}
	
	// PHP's rename() fails on Windows if the target file already exists, 
	// so call our rename_file() which attempts to work around that by
	// deleting the target file and trying again if the initial rename 
	// fails. Not an atomic update, but the best we can do. Errors 
	// encountered while doing that will call self::quit, so just call
	// rename_file() and rely on it to deal with errors.

	self::rename_file($toutfile, $outfile);
	self::rename_file($ttsfile, $tsfile);
    }    

    // Ucam_RSSfetch::get_feed($args) - a static (class) method to fetch data
    // from an RSS feed, with mandatory and optional details passed as an
    // array of keyword/value pairs, seen here as $args.
    //
    // Valid arguments and their default values are identified by entries in
    // $valid_args. NULL value implies required option (use something else, e.g.
    // '', 0, or FALSE as appropriate, for optional "null-ish" values).
    //
    // Returns TRUE if the feed data is successfully fetched and saved, FALSE
    // (with an error message) if an error is encountered and it's unsafe to
    // continue. For some problems with the RSS data (e.g. missing elements that
    // we require), an item may be skipped (with a warning message), possibly
    // leading to a valid output file but with no item details, if the problem
    // affects all items in the feed.
    //
    // The valid arguments are:
    //  * url		(mandatory) the full URL of the RSS news feed.
    //  * directory 	(mandatory) the full path (including drive letter, 
    //			if used on Microsoft Windows) of the directory in 
    //                  which the data saved from an RSS feed should be stored.
    //  * tag           (mandatory) a tag (valid as a filename, e.g. 
    //			'ucsnews.csx-all" for the "all" feed from 
    //                  ucsnews.csx.cam.ac.uk.
    //  * umask		a Unix-style file protection mask, which should be
    //                  sufficient to allow reading and writing by scripts
    //                  using this class both when run by the user under which
    //                  the normal cron jobs or equivalent are run and also 
    //                  any users who may legitimately run the script e.g. for
    //                  testing. The default value (002, octal) allows reading
    //                  by anyone & writing by both the owner of the data file 
    //                  (normally its creator) and by members of the file's 
    //                  group (which, to be useful, should include both the 
    //                  web server user and the other legitimate users as 
    //                  its members). NB Probably won't do anything useful
    //			on Microsoft Windows.

    public function get_feed($args)
    {

	// Define valid arguments (and defaults, if not mandatory) for
	// get_feed(). Mandatory arguments are distinguished by NULL values.
	
	$valid_args = array (
	    'directory' => NULL,	// required, directory for storing data
	    'url' => NULL,		// required, RSS feed URL
	    'tag' => NULL,		// required, tag for referencing feed
	    'umask' => 002,		// optional, data file protection mask
	    );


	if (! is_array($args))
	{
	    self::warn("called with non-array value");
	    return FALSE;
	}

	$ok = TRUE;			// so we can report all errors with
	                                // the arguments before giving up
	$config = $valid_args;		// start off with the defaults
	foreach ($args as $key => $value)
	{
	    if (! array_key_exists($key, $valid_args))
	    {
		self::warn("called with unrecognised option '$key'");
		$ok = FALSE;		// will be reported later
	    }
	    else
	    {
		$config[$key] = $value;
	    }
	}
	
	// Check that all mandatory (NULL default) options have been specified.
	foreach ($valid_args as $key => $value)
	{
	    if ($value === NULL)	// option required, if *really* NULL
	    {
		if ($config[$key] === NULL)	// error if still *really* NULL
		{
		    self::warn('called without required ' . 
			       "option '$value'");
		    $ok = FALSE;	// so we can continue now, quit later
		}
	    }
	}
	if (! $ok) return FALSE;	// exit with error status
	
	umask($config['umask']);	// set protection for output file
	
	// Construct the relevant file names.
	$outfile = $config['directory'] . '/' . $config['tag'];	// target file
	$toutfile = $outfile . '.new';	// temporary, intermediate output file
	$tsfile = $outfile . '.timestamp'; // to contain timestamp for this file
	$ttsfile = $tsfile . '.new';	// temporary timestamp file
	
	// If a timestamp file exists for this feed, get the timestamp
	// timestamp (as Unix-style seconds since January 1 1970 00:00:00
	// GMT), otherwise 0. EXCEPT that if the corresponding feed data file
	// is missing, ignore the timestamp as we need to fetch the feed
	// regardless. [The timestamp is stored as data in the file (rather
	// than e.g. setting the data file's last modification timestamp) as
	// it should be more reliable cross-platform. (Specific Windows
	// issues prompted this approach.)]
	
	$old_timestamp = 0;
	if (file_exists($outfile))
	{
	    if (file_exists($tsfile))
	    {
		$fh = @fopen($tsfile,'r');
		if ($fh)
		{
		    $line = @fgets($fh);
		    if ($line === FALSE)	// input error or unexpected eof
		    {
			if (! feof($fh))
			{
			    self::warn("error reading from '$tsfile': " . 
				       "$php_errormsg - continuing");
			}
			$line ="0\n";	// treat explicitly as missing
		    }
		    
		    if (preg_match('/^(\d+)$/', $line, $match))
		    {
			$old_timestamp = $match[1];	// save for comparison
		    }
		    else
		    {
			self::warn("'$tsfile' did not contain " . 
				   'valid timestamp (but continuing');
		    }
		    @fclose($fh);	// can't do anything about it if error
		}
		else
		{
		    self::warn("unable to open '$tsfile for reading'" . 
			       ' (but continuing): ' . $php_errormsg);
		}
	    }
	}
	
	// Attempt to fetch the feed data (expecting RSS2-format XML). If we
	// have a timestamp for an old copy of the data, use a get-if-modified
	// request so that with luck, we'll just get a 304 Not Modified
	// response if the feed hasn't changed.

	$url = $config['url'];		// for convenience
	$ch = curl_init($url);		// get cURL handle for request
	$new_timestamp = 0;		// for timestamp from response headers
	if ($old_timestamp > 0)
	{
	    // We know when the document was last modified, so fetch and
	    // process it only if it's newer. Otherwise, just fetch
	    // regardless.

	    curl_setopt($ch, CURLOPT_TIMECONDITION, CURL_TIMECOND_IFMODSINCE);
	    curl_setopt($ch, CURLOPT_TIMEVALUE, $old_timestamp);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	// return data as string
	curl_setopt($ch, CURLOPT_HEADER, 1);		// return headers
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);	// wait 10s for connect
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);		// wait 20s max overall
	
	$xml = @curl_exec($ch);		// send a request to the feed URL
	if (! $xml) 
	{
	    $error = curl_error($ch);
	    self::warn("request to '$url' failed: $error");
	    return FALSE;
	}
	
	curl_close($ch);
	
	// We asked for the headers + document body content, which should
	// appear in that order with an empty line between. Separate them.
	
	if (preg_match('/^(.*?\r?\n)\r?\n(.*$)$/s', $xml, $match))
	{
	    $headers = $match[1];
	    $xml = $match[2];
	}
	else 
	{
	    self::warn("the HTTP response for '$url' is unintelligible");
	    return FALSE;
	}
	
	// The response should start with something like HTTP/1.1 200 - we
	// need to extract the last part (HTTP status). If the line's missing,
	// either the response is malformed or it's an antique (HTTP 0.9)
	// response with no headers and no way to tell if it worked - that's
	// not supported here!
	
	if (preg_match('/^HTTP\/\d+\.\d+\s+(\d+)(\s|\r?\n)/', $headers, $match))
	{
	    // If the status is 304 (Not Modified), our current data file is
	    // up-to-date (assuming the timestamp matches the data and nothing
	    // strange has happened!), so we don't need to do anything - just
	    // return success. 
	    //
	    // Otherwise, we require status 200 (Success - should then have
	    // document body comprising the XML data for the RSS feed), and
	    // anything else is treated as failure.
	    
	    $status = $match[1];
	    if ($status == '304')
	    {
		return TRUE;
	    }
	    elseif ($status != '200')
	    {
		self::warn("unexpected HTTP status $status for '$url'");
		RETURN false;
	    }
	}
	else
	{
	    self::warn("no HTTP headers seen in response from '$url'");
	}
	
	// If the response headers include a last modification timestamp, save
	// that for use in future get-if-modified requests. NB We need to
	// convert the timestamp value to the usual UNIX
	// seconds-since-1Jan1970 format.
	
	$new_timestamp = 0;		// save default if no timestamp header 
	if (preg_match('/^Last-Modified:\s+(.*?)$/m', $headers, $match))
	{
	    $new_timestamp = strtotime($match[1]);
	}
	
	// While simplexml_load_file could be used with a URL as the "file",
	// using simplexml_load_string allows us to see the HTTP headers,
	// hence the approach used. 
	//
	// It will fail if the XML is malformed; otherwise, we do the best we
	// can with the data derived from the XML. We don't validate against a
	// DTD since there is no "official" DTD for RSS - it's defined
	// primarily by sometimes-vague textual description, and in any case
	// we couldn't do anything to recover if the document didn't match one
	// of the reverse-engineered DTDs that can be found on the web.
	//
	// Though not explicitly stated, it looks as though the SimpleXML 
	// parsing functions will always return data encoded as UTF-8, and
	// if necessary convert from the actual encoding of the input file
	// (as long as that's declared correctly in the 
	// <? xml version="..." encoding="..."> line). Knowing the encoding is 
	// important for correctly encoding the text for use in HTML. For the
	// RSS feeds from www.admin.cam.ac.uk and ucsnews.csx.cam.ac.uk (at
	// the time of writing these comments...) the RSS data is actually
	// encoded as UTF-8, so it should be safe even if SimpleXML copied 
	// the data through without checking and adjusting the encoding.

	if (! $data = @simplexml_load_string($xml))
	{
	    self::quit("loading the XML from '$url' failed: $php_errormsg");
	}

	// Check that the outermost XML element is RSS, as expected, and that
	// it has an associated version which is the one expected, since
	// there's no guarantee that this would work for anything else. NB The
	// evolution of RSS led to a situation in which a higher version
	// number does not simply mean newer (but probably compatible) - there
	// was far greater divergence between versions.
	
	$outer_element = $data->getName();
	if ($outer_element != 'rss')
	{
	    self::warn("outer XML element is not <rss> in XML from '$url'");
	    return FALSE;
	}
	
	$expected_rss_version = '2.0';
	$rss_version = $data->attributes()->version;
	if ($rss_version != $expected_rss_version)	// stop if wrong version
	{
	    self::warn("RSS version is $rss_version " . 
		       "($expected_rss_version expected) in XML from '$url'");
	    return FALSE;
	}
	
	// Get details of XML namespaces defined in the document and see if
	// ucamadmin namespace (as used for the www.admin.cam.ac.uk RSS feed
	// per-item images) is among them. If it is, expect per-item imageurl
	// and imagetext (image description, cf HTML alt text) elements in
	// that namespace.

	$ucamadmin_ns_found = FALSE;
	$namespaces = $data->getDocNamespaces(true);

	if (array_key_exists('ucamadmin',$namespaces) and 
	    $namespaces['ucamadmin'] != '')
	{
	    $ucamadmin_ns_found = TRUE;
	    $ucamadmin_ns_url = $namespaces['ucamadmin'];
	}
	
	// Accumulate the data in $info and write it later, when complete.

	$info = '';
	
	// Add the data file "header" - e.g. format version & source URL.
	
	$info .= "RSS news format: 1\nURL: $url\n\n";
	
	if (! isset($data->channel) or ! isset($data->channel->item))
	{
	    // No <channel> and/or <item>, so there cannot be any stories.
	    // The feed is either just broken, or (entirely reasonable for
	    // some feeds) currently lacks any items and the outer elements
	    // have been omitted in consequence.
	    //
	    // Write a data file with just the header information, no items.
	    // Use zero timestamp to avoid get-if-modified for future runs,
	    // which might leave the data broken unnecessarily e.g. if the 
	    // feed was broken but sending "now" as the timestamp, then 
	    // reverted to sending the correct (older) timestamp when fixed.

	    self::save_data($info, 0, $outfile, $toutfile, 
			    $tsfile, $ttsfile);
	    return TRUE;		// success, though no items!
	}

	// To guard against feeds including a vast numebr of truly ancient
	// items, far more than anyone's likely to want in a "news box",
	// impose a limit.

	$max_item = 10;
	
	// Step through the feed items (assumed to be newest-first), saving the
	// details in the output file.
	//
	// Note that many SimpleXML accessors are actually iterators, which
	// may return multiple values (if the XML had multiple instances of a
	// child element) if used in an appropriate context (e.g. foreach
	// loop), but which will simply return the first from simple
	// use. 
	//
	// Since we expect single values for element of interest, and it would
	// be "unclear" what to do if we found multiple values, we simply
	// fetch the first or only value for each, and use that.

	$nitem = 1;		       	// item number (for msgs & limit check)
	foreach ($data->channel->item as $item)
	{
	    // Check all the information we need exists (before using it!) and
	    // is not null string or solely whitespace characters. Moan and
	    // ignore the item if anything's missing.
    
	    if ((! isset($item->link) or trim($item->link) == '') or 
		(! isset($item->title) or trim($item->title) == '') or 
		(! isset($item->description) or trim($item->description) == '') or
		(! isset($item->pubDate) or trim($item->pubDate) == ''))
	    {
		self::warn("incomplete RSS data from '$url': missing link, " .
			   "title, text or date for item $nitem - ignored");
		return FALSE;
	    }

	    $link = trim($item->link);
	    $title = trim($item->title);
	    $text = trim($item->description);
	    $date = trim($item->pubDate);
	    $imageurl = '';	       	// URL for per-item image
	    $imagetext = '';		// description (HTML alt text) for image
	    if ($ucamadmin_ns_found)
	    {
		$item_children = $item->children($ucamadmin_ns_url);
		if ($item_children)
		{
		    $imageurl = trim($item->children($ucamadmin_ns_url)->imageurl);
		    $imagetext = trim($item->children($ucamadmin_ns_url)->imagetext);
		}
		
		if ($imageurl == '' or $imagetext == '')
		{
		    self::warn("incomplete RSS data from '$url': missing " . 
			       "imageurl or imagetext for item $nitem - ignored");
		    // Uncomment the following line if you just want to 
		    // give up (leaving any old data file in place, and 
		    // potentially with stale content) if the feed's empty
		    // (whether legitimately or due to an error).

		    // return FALSE;
		}
	    }
	    
	    // As long as the date's in the expected format (as seen in the
	    // feeds from ucsnews.csx.cam.ac.uk and www.admin.cam.ac.uk),
	    // format it to be more suitable for use in web pages. 
    
	    if (! preg_match('/^\w\w\w, (\d\d) (\w\w\w) (\d\d\d\d) ' .
			     '\d\d:\d\d:\d\d (GMT|\+\d{4}|-\d{4})$/', 
			     $date, $match))
	    {
		self::warn("date '$date' not in expected variant of RFC822 " .
			   "format for item $nitem from '$url' - ignored");
		return FALSE;
	    }
	    
	    $date = "{$match[1]} {$match[2]} {$match[3]}";

	    // Convert all the text to have HTML character entities (&amp;,
	    // &123;, etc.) for any non-ASCII characters, so the data from
	    // this file can be used verbatim in web pages. Also, since the
	    // data file requires that each item occupy a single line with TAB
	    // as field separator, ensure that any TABs and newlines are
	    // converted to single spaces (which should make no difference to
	    // the rendered HTML), except that in URLs ($link, $imageurl) such
	    // characters are simply discarded as extraneous junk).
	    //
	    // This involves knowledge of the character encoding used in the
	    // data, but SimpleXML appears to return all data in UTF-8, which
	    // so specify that. If the encoding differs from that (probably
	    // due to an encoding or labelling error at the RSS feed source),
	    // the output may be garbled but should at least be safe.
	    
	    $link = htmlentities($link, ENT_QUOTES, 'UTF-8');
	    $link = preg_replace('/[\n\r\t]+/sm', '', $link);
	    
	    $title = htmlentities($title, ENT_QUOTES, 'UTF-8');
	    $title = preg_replace('/[\n\r\t]+/sm', ' ', $title);
	    
	    $date = htmlentities($date, ENT_QUOTES, 'UTF-8');
	    $date = preg_replace('/[\n\r\t]+/sm', ' ', $date);

	    $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
	    $text = preg_replace('/[\n\r\t]+/sm', ' ', $text);
	    
	    $imageurl = htmlentities($imageurl, ENT_QUOTES, 'UTF-8');
	    $imageurl = preg_replace('/[\n\r\t]+/sm', '', $imageurl);
	    
	    $imagetext = htmlentities($imagetext, ENT_QUOTES, 'UTF-8');
	    $imagetext = preg_replace('/[\n\r\t]+/sm', ' ', $imagetext);

	    $info .= "$link\t$title\t$date\t$text\t$imageurl\t$imagetext\t\n";
	    
	    // Ignore any further items once we've got the most we'll ever
	    // be interested in using.

	    if ($nitem++ >= $max_item) break;
	}

	// Create the corresponding data files. save_data() will terminate
	// execution in the event of problems doing that.

	self::save_data($info, $new_timestamp, $outfile, $toutfile, 
			$tsfile, $ttsfile);
	return TRUE;
    }
}				// end of class Ucam_RSSfetching definition


class Ucam_RSSformat {

    // This class is intended for use by web pages, either to fetch data
    // previously saved from an RSS news feed by Ucam_RSSfetch for use by 
    // the caller, or to return the data as text with HTML markup for a 
    // predefined style of news box.

    var $info = array();		// overall info about the RSS feed
    var $items = array();		// data for individual news items
    var $last_error = NULL;		// the most recent error message text

    // Ucam_RSSformat() - object constructor, returning a new instance of 
    // the Ucam_RSSformat object.

    public function Ucam_RSSformat()
    {
	$this->info = NULL;
	$this->items = NULL;
	$this->last_error = NULL;
	return;
    }

    // logmsg($text) is a private method used to log details of problems, with
    // automatic identification of what is reporting the problem (this
    // class/file and the calling function). Also saves the error message as
    // $this->last_error (though that may also be set independently to reflect
    // "routine" oddities that don't warrant logging or might be liable to
    // flood the log file) so our caller can retrieve details to report in
    // other ways.

    protected function logmsg($text)
    {
	$backtrace = debug_backtrace();	// to get name of calling function
	// The first element ([0]) of $backtrace should be details of the 
	// current function - boring, but the next element ([1]) should be
	// our caller, including the function name - though we should check
	// the array elements exist "just in case".

	if (array_key_exists(1, $backtrace) and 
	    array_key_exists('function', $backtrace[1]))
	{
	    $caller = $backtrace[1]['function']; // our caller's name
	}
	if ($caller != '') $caller .= ': ';
	$msg = __FILE__ . ": $caller" . $text;
	$this->last_error = $msg;
	error_log($msg);
	return;
    }

    // last_error() is a public method which returns either the last error
    // message string saved by one of the data-fetching methods before that
    // returned FALSE to indicate an error had occurred, or NULL if no error
    // has been recorded. NB The same error message should already have been 
    // logged using logmsg(), so there's no need to call last_error() if the
    // only use that would be made of the result is logging it!
    //
    // You probably SHOULD NOT report the result in a web page (except for
    // convenience during private testing), since it will be meaningless to
    // the user and is liable to include file names & directory paths, and
    // revealing those to potential attackers is generally considered a bad
    // idea.

    public function last_error()
    {
	return $this->last_error;
    }

    // get_rssdata($directory, $tag) - public method to fetch RSS data for the
    // feed identified by $tag from a data file in directory $directory. For
    // example, 
    //
    // $result = $rss->get_rssdata('/the/directory/name', 'the-feed-tag');
    //
    // If successful, returns a two-element array - see below for details.
    //
    // On failure, returns FALSE and the last_error() method can be used to
    // retrieve an error message (which will also have been logged using
    // logmsg()).
    //
    // When successful, the first element (subscript 0) of the result array
    // has as its value an array comprising key/value pairs (i.e. as string
    // subscripts and the corresponding array element values) for overall
    // information about the RSS feed. The items which may appear there are:
    //
    // 'dataversion'	the version number of the data file format; primarily
    //			only useful during testing.
    // 'rssurl'		the URL of the RSS feed; may be used to provide a link
    //			back to the feed source (e.g. from an "RSS feed" icon).
    //			[Already entity-encoded ready for use in HTML.]
    //
    // The second element (subscript 1) of the result array has as its value
    // an array (with numeric subscripts from 0 upward) in which each value is
    // itself a further level of sub-array, corresponding to a single item
    // (news story etc.) from the RSS feed, as keyword/value (array
    // subscript/element value) pairs. The order of items is the same as in
    // the RSS feed, normally newest first.
    //
    // NB The Ucam_RSSfetch class imposes an upper limit on the number of 
    // items for which it will save details, with any further items ignored.
    // Items which lack required elements in the original data will have been
    // ignored by Ucam_RSSfetch, which may explain unexpectedly missing items.
    //
    // In the sub-array for a particular item, the following keys (subscripts)
    // and values should be present (though possibly with null strings as
    // their values):
    //
    // 'url'		the URL for linking to the full item.
    // 'title'		the item's title.
    // 'pubdate'	the item's publication date (e.g. "12 May 2008").
    // 'text'		the item's text (typically the first paragraph of 
    // 			of the full text from the item, but that's determined
    //			by whatever is included in the feed).
    //
    // Two special-case items, available only for the feeds from
    // www.admin.cam.ac.uk (which uses a local RSS extension to add them):
    //
    // 'imageurl'	the URL of an image to display alongside the other
    //			details for the item. Null string when not available.
    // 'imagedesc'	description of the image (potentially for use in an 
    //			<img> element's alt= attribute), though at the time
    //			writing it is always just a copy of the title. Null
    //			string when not available.
    // 
    // NB The corresponding values are already entity-encoded ready for use
    // in HTML without further encoding (assuming the character encoding for
    // the page in which they are being used is a superset of ASCII), but do
    // not include any other HTML markup.

    public function get_rssdata($directory=NULL, $tag=NULL)
    {
	// Initialise the object variables so that, in particular, we don't
	// add new items to the end of an array (where they'' mnost likely
	// be ignored) if it already containing existing items from an earlier
	// call using the same object.

	$this->info = array();
	$this->items = array();
	$this->last_error = NULL;

	// We can't judge whether the argument values are valid, but we can at
	// least check they were supplied and not NULL (or null string).
	if (NULL === $directory or NULL === $tag or '' == $directory or 
	    '' == $tag)
	{
	    $this->logmsg('directory and/or tag missing or invalid');
	    RETURN FALSE;
	}

	// Derive data file's name from the supplied directory and tag.
	$file = "$directory/$tag";

	$fh = @fopen($file, 'rt');
	if (! $fh)
	{
	    $this->logmsg("failed to open $file for reading: $php_errormsg");
	    return FALSE;
	}
	
	// Subsequent lines, up to a blank line, should be additional
	// information about the feed. Save recognised values from those for
	// later; unknown values are assumed to be newly-defined extra values,
	// and ignored.
	
	while ($line = @fgets($fh) and $line != "\n")
	{
	    if (preg_match('/^([^:]+):\s+(.*?)\s*$/', $line, $match))
	    {
		$key = strtolower($match[1]);	// lowercase for safe comparison
		$value = trim($match[2]);
		if ($key == 'rss news format') 
		{ 
		    $this->info['dataversion'] = htmlentities($value,
							      NULL, 'UTF-8'); 
		}
		elseif ($key == 'url') 
		{ 
		    $this->info['rssurl'] = htmlentities($value, ENT_QUOTES, 
							 'UTF-8');
		}
		else 
		{ 
		    $this->logmsg("unknown info field ($key) " .
				  "in '$file' - ignored"); 
		}
	    }
	}
	
	// Loop may have ended normally or ($line set to FALSE) because of 
	// an error...

	if ($line === FALSE and ! feof($fh))
	{
	    $this->logmsg("reading from $file failed: $php_errormsg");
	    @fclose($fh);		// close file before returning
	    return FALSE;
	}

	// Check that mandatory "header" values are present; without them, we
	// cannot usefully proceed.
	
	foreach (array('dataversion' => 'RSS News Format', 'rssurl' => 'URL') 
		 as $key => $value)
	{
	    if (! array_key_exists($key, $this->info) or $this->info == '')
	    {
		$this->logmsg("data file '$file' lacks required " . 
		    "header field '$value'");
		return FALSE;
	    }
	}
	
	while ($line = fgets($fh))
	{
	    // Each line after the first should comprise several TAB-delimited
	    // fields:
	    //  * link URL
	    //  * item title
	    //  * item publication date 
	    //  * first or only paragraph of the item's text (spaces, TABs, & 
	    //    newlines collapsed to  single spaces). 
	    //  * URL of image file associated with the item 
	    //    (www.admin.cam.ac.uk feeds only - using RSS extension - 
	    //    else null field)
	    //  * image description (for alt=, www.admin.cam.ac.uk feeds only -
	    //    using RSS extension - else null field)
	    //
	    // To ease the transition if extra fields are added in future, 
	    // accept (but ignore) any unexpected fields - but moan if there are
	    // too few as they should all exist (even if empty).

	    $fields = explode("\t", $line);
	    $nfield = count($fields);
	    
	    if ($nfield < 6)		// should be at least 6 fields!
	    {
		$this->logmsg("too few fields ($nfield) " . 
			      "in RSS data line in $file - skipped");
		continue;
	    }

	    // For convenience and to reduce the risk of muddles (using the
	    // wrong subscript), save the values using names rather than
	    // numeric subscripts.

	    $row = array();
	    $row['url'] = $fields[0];
	    $row['title'] = $fields[1];
	    $row['pubdate'] = $fields[2];
	    $row['text'] = $fields[3];
	    $row['imageurl'] = $fields[4];
	    $row['imagedesc'] = $fields[5];

	    $this->items[] = $row;
	}
	
	// Loop may have ended normally or ($line set to FALSE) because of 
	// an error...

	if ($line === FALSE and ! feof($fh))
	{
	    $this->logmsg("reading from $file failed: $php_errormsg");
	    @fclose($fh);		// close file before returning
	    return FALSE;
	}

	if (! @fclose($fh))
	{
	    $this->logmsg("error closing $file after reading: " . 
		"$php_errormsg");
	    return FALSE;
	}

	// Success! Return a two-element array, with the info and items
	// arrays as sub-arrays.

	return array($this->info, $this->items);
    }

    // format_rssdata() is a public method returning an HTML-formatted news
    // box based on data from the current content of a specified RSS feed.
    //
    // If successful, it returns a string comprising the newsbox, ready for
    // inclusion in a web page (with all required encoding of special
    // characters already done). If it fails, it returns FALSE and the 
    // last_error() method can be used to retrieve a copy of the error message
    // (which will also have been logged).
    //
    // NB This method calls get_rssdata() itself. Consequently, errors from
    // get_rssdata may be seen, as well as errors originating in this method.
    // 
    // The HTML is intended to be suitable for use in XHTML 1.0 (transitional
    // or strict) web pages and would probably work in other cases, though
    // not necessarily validating cleanly. All non-ASCII characters should be
    // encoded as the corresponding &...; character entity, so it should 
    // also work regardless of the declared character encoding of the web page
    // if the actual encoding is a superset of ASCII.
    //
    // Rather than having a large number of positional arguments (many unused),
    // the arguments are supplied as a single array variable or using array(),
    // as in array('somekey' => "the key's value, 'key2' => 'value2'). 
    //
    // Note that if "unset" optional values are specified explicitly, they
    // must be specified as a null string (''), not as NULL.
    //
    // IMPORTANT: for all URLs and other string values that will be used in 
    // the generated HTML, whether visibly or "behind the scenes", it is 
    // essential that the supplied values are encoded appropriately. More
    // specifically:
    //
    // For URLs: any characters which are "unsafe", and any characters that
    // have special meaning in URLs *but* are intended literally ("as
    // themselves"), require the usual hexadecimal encoding such as %20 for a
    // literal space.
    // 
    // All text values that will be used in the HTML (including URLs, after
    // any required hex-encoding) required HTML character entity encoding
    // (&amp; etc.) for (a) literal characters that would otherwise be special
    // in HTML (such as <, &, and >) and (b) any non-ASCII characters
    // (e.g. pound sign, accented characters, etc.).  HTML markup (<b> etc.)
    // must not be used, only HTML character entity encoding.
    //
    // Where such encoding is needed, it is indicated below as that a value
    // must be "HTML-encoded" or "URL+HTML encoded".
    //
    // The currently available arguments are:
    //
    // Mandatory arguments (required for all use of this method):
    // 'directory' 	The full path (including drive letter, on MS Windows)
    //			of the directory containing the saved RSS feed data.
    //                  A simple text string, no encoding.
    // 'tag'            The identifying "tag" for the particular feed (which 
    //			also the filename within the data directory). A simple
    //                  text string, no encoding.
    // 'heading'        the heading text for the news box title bar. Must be
    //                  HTML-encoded.
    // 'type'           the type of news box to be generated, identified by 
    //                  A simple string value (no encoding); the 
    //                  currently-available choices are
    //			 'v2' - basic news box, no images; should work
    //                            for feeds from ucsnews.csx.cam.ac.uk and 
    //                            www.admin.cam.ac.uk. As used in the old (V2)
    //                            house style for CS news feeds.
    //			 'v2+image' - a slightly fancier version which 
    //                            displays the first item differently, with
    //                            the details beside an image; reverts to 
    //                            same as "v2" if no image URL available.
    //                            NB Only the www.admin.cam.ac.uk feeds include
    //                            per-item image details. As used in the old
    //                            (V2) house style for www.admin news feeds.
    //                   'v3' - as used with the V3 house style; no item text
    //                            or image.
    //                   'v3+image' - as used with the V3 house style; includes
    //                            item image for first item, but not text.
    //                   'v3+image+text' - as used with the V3 house style;
    //                            includes item image and text for first item.
    //
    // Optional arguments:
    // 'class_prefix'   A string prefix to be used for the CSS class names in
    //                  the generated HTML, so that different news boxes can
    //                  use different CSS styling. Defaults to "news_", hence
    //                  class names such as "news_box" and "news_item_title".
    //                  Must be HTML-encoded.
    // 'headingurl'     controls whether the news box heading is a link to
    //                  somewhere. May be boolean TRUE (link heading to
    //                  to the overall (web page) URL specified in the RSS feed;
    //                  boolean FALSE (default, suppress heading link; or
    //                  a URL (URL+HTML encoded) which will be used as target
    //                  of the heading link.
    // 'nonewstext'     Text to be displayed in place of the items if there
    //			are no items in the feed or they were all deemed 
    //                  invalid (hence ignored). HTML-encoded. If not supplied,
    //                  the news box will be empty apart from a "See also"
    //                  link if 'seealsourl' is defined.
    // 'maxitems'       The maximum number of items to display; integer, 
    //                  default 4. Upper limit determined by the number of 
    //                  items for which details are saved in the data file.
    //                  The default will be used if zero or negative value
    //                  specified.
    // 'rsslogourl'     URL for the "RSS logo" image file (for inclusion in
    //			the news box title bar, linking to the 
    //                  underlying feed). Default is null string, which 
    //                  suppresses inclusion of the logo and link. Must be
    //                  URL+HTML-encoded.
    // 'rssalt'         <img alt=...> description for the RSS feed; must be 
    //                  HTML-encoded. Default is '[rss feed]'.
    // 'rsstitle'       <img title=...> description for the RSS feed (useful
    //                  for accessibility and browser "tooltips"); must be 
    //                  HTML-encoded. Default is 'RSS feed'.
    // 'seealsodesc'    Description for "See also" link at bottom of news box.
    //                  Only used if 'seealsourl' enables it, and may be 
    //                  null string (in which case the URL will be shown as
    //                  the link description). Must be HTML-encoded. The 
    //                  supplied text will be preceded by "See also ".
    // 'seealsourl'     controls whether a "see also" link is added after the
    //                  news items. May be boolean TRUE (include "see also",
    //                  linking to the overall web page URL specified in the RSS
    //                  feed; boolean FALSE (default, suppress "see also" link;
    //                  or a URL (URL+HTML encoded) which will be used as target
    //                  of the "see also" link.

    public function format_rssdata($args)
    {
	// Initialise $this->last_error so we don't inadvertently return 
	// a stale error from an unrelated call.

	$this->last_error = NULL;

	// Define valid arguments (and defaults, if not mandatory).

	$valid_args = array(
	    'directory' => NULL,
	    'tag' => NULL,
	    'heading' => NULL,
	    'type' => NULL,
	    'class_prefix' => 'news_',
	    'headingurl' => FALSE,
	    'maxitems' => 4,
	    'nonewstext' => '',
	    'rsslogourl' => '',
	    'rssalt' => '[rss feed]',
	    'rsstitle' => 'RSS feed',
	    'seealsodesc' => '',
	    'seealsourl' => FALSE,
	    );

	// Define valid news box types and associated style configuration. 
	// Type is key, style config for type is value.

	$style_config = array( 
	    'v2' => 		array('version' => 'v2', 
				      'image' => FALSE, 'text' => FALSE), 
	    'v2+image' => 	array('version' => 'v2', 
				      'image' => TRUE, 'text' => FALSE), 
	    'v3' => 		array('version' => 'v3', 
				      'image' => FALSE, 'text' => FALSE), 
	    'v3+image' => 	array('version' => 'v3', 
				      'image' => TRUE, 'text' => FALSE), 
	    'v3+image+text' => 	array('version' => 'v3', 
				      'image' => TRUE, 'text' => TRUE), 
	    );

        if (! is_array($args))
        {
	    $this->logmsg("called with non-array value");
            return FALSE;
        }

	// Check that all the supplied arguments are valid, expected ones,
	// building in $config a complete set of supplied (where available)
	// or default values.
	// 
	// Log problems and note that all is not well, but continue the checks
	// so that all problems with missing/unknown arguments can be found
	// from the log entries for a single run.

        $ok = TRUE;                     // so we can report all errors
        $config = $valid_args;          // start off with defaults
        foreach ($args as $key => $value)
        {
            if ( array_key_exists($key, $valid_args))
            {
                $config[$key] = $value;
            }
            else
            {
		$this->logmsg("called with unrecognised option '$key'");
                $ok = FALSE;            // will be reported later
            }
        }

        // Check that all mandatory (NULL default) options have been specified.

	$opt = $args;
        foreach ($valid_args as $key => $value)
        {
            if ($value === NULL)        // option required, if *really* NULL
            {
                if ($config[$key] === NULL)     // error if still *really* NULL
                {
		    $this->logmsg("called without required option '$key'");
                    $ok = FALSE;        // so we can continue now, quit later
                }
            }
        }
        if (! $ok) 
	{
	    // Error(s) already logged, last error detected already saved 
	    // as last_error.
	    return FALSE;        // exit with error status
	}

	// If get_rssdata() is successful, it will return a 2-element array
	// with the info and items arrays as the elements' values.
	$data = $this->get_rssdata($config['directory'], $config['tag']);
	if (! $data)
	{
	    // No error message here, as get_rssdata will already have
	    // logged the problem and set last_error.
	    return FALSE;
	}

	// Use named scalars for convenient access, rather than array elements.
	$info = $data[0];
	$items = $data[1];

	// Check type is a known value.

	$s_config = $style_config[$config['type']];

	if (! $s_config)
	{
	    $this->logmsg('missing or unrecognised \'type\' value ' . 
			  "({$config['type']})");
	    return FALSE;
	}

	// Make style configuration for the chosen type available as scalars.

	$s_version = $s_config['version'];
	$s_image = $s_config['image'];
	$s_text = $s_config['text'];

	// Derive CSS class names, using the supplied or default prefix. 

	$class_prefix = $config['class_prefix'];
	$main_class = $class_prefix . 'box';
	$rss_image_class = $class_prefix . 'rss_image';
	$item_image_class = $class_prefix . 'item_image';
	$item_class = $class_prefix . 'item';
	$item_title_class = $class_prefix . 'item_title';
	$item_date_class = $class_prefix . 'item_date';
	$clear_class = $class_prefix . 'clear';
	$seealso_class = $class_prefix . 'seealso';
	$item_more_class = $class_prefix . 'item_more';
	$item_right_link_class = $class_prefix . 'item_right_link';

	if ($s_version == 'v2')		// v2 house style
	{
	    $text = '';
	    $text .= "<div class=\"$main_class\">\n";
	    if ($config['rsslogourl'] != '')
	    {
		$text .= "<a href=\"{$info['rssurl']}\">" .
		    "<img class=\"$rss_image_class\" " .
		    "src=\"{$config['rsslogourl']}\" " .
		    "alt=\"{$config['rssalt']}\" " .
		    "title=\"{$config['rsstitle']}\" " .
		    "width=\"16\" height=\"16\" /></a>\n";
	    }

	    // The headingurl option can be TRUE (use URL from feed data),
	    // FALSE (default, don't link), or a string which is assumed to be
	    // a ready- encoded URL that should be linked. NB TRUE/FALSE must
	    // be the boolean values, not values of other types that evaluate
	    // as true or false.

	    $headingurl = $config['headingurl'];
	    if ($headingurl === TRUE) 
	    {
		$headingurl = $info['url'];
	    }
	    if (is_string($headingurl))
	    {
		$text .= "<h2><a href=\"$headingurl\">{$config['heading']}</a></h2>\n";
	    }
	    else
	    {
		$text .= "<h2>{$config['heading']}</h2>\n";
	    }
	    
	    if (count($items) > 0)
	    {
		$showdate = TRUE;
		$numtodo = $config['maxitems'];	// count down from max items
		if ($numtodo <= 0)	// use default if supplied value silly
		{
		    $this->logmsg("invalid 'maxitems' ($numtodo) - " . 
				  "using default");
		    $numtodo = $valid_args['maxitems'];
		}
	    
		// If style requires image, it is expected that an image URL
		// will be available, and the first item is formatted
		// differently with the corresponding image included. If an
		// image URL is not available (value is null string), the
		// special handling will be skipped and the result will be the
		// same as for when the image is not enabled.

		if ($s_image and $items[0]['imageurl'] != '')
		{
		    // When showing an image, the date for that item is shown
		    // beside the image and the date is not shown for other
		    // items.
		    
		    $showdate = FALSE;
		    
		    $text .= "<div class=\"$item_image_class\">\n";
		    
		    // For convenience, get the details for the first story as
		    // a simple array, shifting it off the array so it won't be
		    // repeated.
		    
		    $item0 = array_shift($items);
		    $text .= "<a href=\"{$item0['url']}\">" . 
			"<img src=\"{$item0['imageurl']}\" " .
			"alt=\"{$item0['imagedesc']}\" /></a>" . 
			"<br />\n</div>\n";	
		    
		    $text .= "<div class=\"$item_class\">\n";
		    $text .= "<h3>{$item0['pubdate']}</h3>\n";
		    $text .= "<p><span class=\"$item_title_class\">" . 
			"{$item0['title']}</span>\n";
		    $text .= "<a href=\"{$item0['url']}\">more</a>...</p>\n";
		    $text .= "</div>\n";
		    $text .= "<div class=\"$clear_class\">&nbsp;</div>\n";

		    $numtodo--;		// note that one item has been done
		}
	    
		$text .= "<ul class=\"$item_class\">\n";
		foreach ($items as $item)
		{
		    if ($numtodo <= 0) break; // enough items already done
		    
		    // All items should have url and title, but skip any that
		    // do not.
		    $date = '';		// inserted only if no image+full date
		    if ($item['url'] != '' and $item['title'] != '')
		    {
			if ($showdate) 
			{
			    // For brevity, drop year from in-line dates.
			    $date = $item['pubdate'];
			    $date = preg_replace('/\s+\d{4}$/', '', $date);
			    $date = "<span class=\"$item_date_class\">" . 
				"$date:</span> "; 
			}
			$text .= "<li><a href=\"{$item['url']}\">$date" . 
			    "<span class=\"$item_title_class\">" .
			    "{$item['title']}</span></a></li>\n";
			$numtodo--;		// and another item done
		    }
		}
		$text .= "</ul>\n";
	    }
	    elseif ($config['nonewstext'])	// no items; text to display?
	    {
		$text .= "<p>{$config['nonewstext']}</p>\n";
	    }

	    // Add a "See also ..." link, if URL provided, with either
	    // supplied text or the link URL as description.
	
	    // The seealsourl option can be TRUE (use URL from feed data),
	    // FALSE (default, don't link), or a string which is assumed to be
	    // a ready- encoded URL that should be linked. NB TRUE/FALSE must
	    // be the boolean values, not values of other types that evaluate
	    // as true or false.

	    $seealsourl = $config['seealsourl'];
	    if ($seealsourl === TRUE) 
	    {
		$seealsourl = $info['rssurl'];
	    }
	    if (is_string($seealsourl))
	    {
		$text .= "<p class=\"$seealso_class\">";
		if ($config['seealsodesc'] != '')
		{
		    $text .= "See also <a href=\"$seealsourl\">" .
			"{$config['seealsodesc']}</a>";
		}
		else
		{
		    $text .= "See also <a href=\"$seealsourl\">" .
			"$seealsourl</a>";
		}
		$text .= "</p>\n";
	    }
	}
	else				// v3 house style (assumed; not v2)
	{
	    {
		$text = '';
		$text .=  "<div class=\"$main_class\">\n" . 
		    "<dl class=\"$main_class\">\n<dt>\n";
		if ($config['rsslogourl'] != '')
		{
		    $text .= "<a href=\"{$info['rssurl']}\">" .
			"<img class=\"$rss_image_class\" " .
			"src=\"{$config['rsslogourl']}\" " .
			"alt=\"{$config['rssalt']}\" " .
			"title=\"{$config['rsstitle']}\" /></a>\n";
		}
      
		// The headingurl option can be TRUE (use URL from feed data),
		// FALSE (default, don't link), or a string which is assumed
		// to be a ready- encoded URL that should be linked. NB
		// TRUE/FALSE must be the boolean values, not values of other
		// types that evaluate as true or false.

		$headingurl = $config['headingurl'];
		if ($headingurl === TRUE) 
		{
		    $headingurl = $info['url'];
		}
		if (is_string($headingurl))
		{
		    $text .= "<a href=\"$headingurl\">" .
			"{$config['heading']}</a>\n";
		}
		else
		{
		    $text .= "{$config['heading']}\n";
		}
		$text .= "</dt>\n";
	    
		if (count($items) > 0)
		{
		$text .= "<dd>\n";
		    $numtodo = $config['maxitems'];	// count down from max items
		    if ($numtodo <= 0)	// use default if supplied value silly
		    {
			$this->logmsg("invalid 'maxitems' ($numtodo) - " . 
				      "using default");
			$numtodo = $valid_args['maxitems'];
		    }
	    
		    // If style requires image, it is expected that an image
		    // URL will be available and the first item is formatted
		    // differently, with the corresponding image included. If
		    // an image URL is not available (value is null string),
		    // the special handling will be skipped and the result
		    // will be the same as when image not enabled.

		    if ($s_image and $items[0]['imageurl'] != '')
		    {
			$text .= "<div class=\"$item_class\">";
			
			// For convenience, get the details for the first
			// story as a simple array, shifting it off the array
			// so it won't be repeated.
		    
			$item0 = array_shift($items);

			$text .= "<p class=\"$item_date_class\">" .
			    "{$item0['pubdate']}</p>\n" . 
			    "<a href=\"{$item0['url']}\">" . 
			    "<img src=\"{$item0['imageurl']}\" " . 
			    "alt=\"\" /></a>\n";

			// When text included, follow it with Read more link,
			// whereas if it's omitted then (a) omit "Read more" 
			// and (b) set style to append simple arrow to title.

			$the_text = '';
			$text_class = '';
			if ($s_text)
			{
			    $the_text = "{$item0['text']}";
			}
			else
			{
			    $text_class = $item_right_link_class;
			}
			$text .= "<p> <a class = \"$text_class\" " . 
			    "href=\"{$item0['url']}\">" .
			    "{$item0['title']}</a><br />" .
			    $the_text . "</p>\n";
			if ($s_text)
			{
			    $text .= "<p class=\"$item_more_class\">" .
				"<a href=\"{$item0['url']}\">" .
				"Read more</a></p>\n";
			}
			$text .= "</div>\n";
			$numtodo--;   	    // note that one item has been done
		    }
	    
		    foreach ($items as $item)
		    {
			if ($numtodo <= 0) break; // enough items already done
			
			// All items should have url and title, but skip any
			// that do not.
			$date = '';		// inserted only if no image+full date
			if ($item['url'] != '' and $item['title'] != '')
			{
			    $text .= "<div class=\"$item_class\">";
			    $text .= "<p class=\"$item_date_class\">" . 
				"{$item['pubdate']}</p>\n" .
				"<p> <a class=\"$item_right_link_class\" " . 
				"href=\"{$item['url']}\">" .
				"{$item['title']}</a></p>";

			    $numtodo--;		// and another item done
			}
			$text .= "</div>\n";
		    }
		}
		elseif ($config['nonewstext'])	// no items; text to display?
		{
		    $text .= "<dd><div class=\"$item_class\">" . 
			"<p>{$config['nonewstext']}</p></div>\n";
		}

		// Add a "See also ..." link, if URL provided, with either
		// supplied text or the link URL as description.
		
		// The seealsourl option can be TRUE (use URL from feed data),
		// FALSE (default, don't link), or a string which is assumed
		// to be a ready- encoded URL that should be linked. NB
		// TRUE/FALSE must be the boolean values, not values of other
		// types that evaluate as true or false.

		$seealsourl = $config['seealsourl'];
		if ($seealsourl === TRUE) 
		{
		    $seealsourl = $info['rssurl'];
		}
		if (is_string($seealsourl))
		{
		    $text .= "<div class=\"$seealso_class\">" ;
		    if ($config['seealsodesc'] != '')
		    {
			$text .= "See also <a href=\"$seealsourl\">" .
			    "{$config['seealsodesc']}</a>";
		    }
		    else
		    {
			$text .= "See also <a href=\"$seealsourl\">" .
			    "$seealsourl</a>";
		    }
		    $text .= "</div>\n";
		}

		$text .= "</dd></dl>\n";
	    }
	}
	
	$text .= "</div>\n";

	return $text;
    }
} 			    	// end of class Ucam_RSSformat definition


?>
