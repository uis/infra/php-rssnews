<?php

error_reporting(E_ALL);                 // moan about any/all problems
ini_set('track_errors', TRUE);          // so $php_errormsg can be used
ini_set('html_errors', FALSE);	        // no HTML markup in error messages

// Tell PHP where to find the library classes. Adjust as required.
include_once '/site/admindir/lib/ucam_rssnews.php';

// The directory in which data from the RSS feed will be stored. Adjust as
// required.
$directory = '/site/admindir/rss-data';

// The location of the RSS feed icon image.
$rssicon = '/images/srssfeed.gif';

// Retrieve and format data for the RSS feed identified by tag 
// 'ucsnews.csx-all', 

$tag = 'ucsnews.csx-all';
$rss = new Ucam_RSSformat();

$result1 = $rss->format_rssdata(
    array(
	'directory' => $directory,
	'tag' => $tag,
	'type' => 'v2',
	'heading' => 'University Computing Service news',
	'headingurl' => 'http://ucsnews.csx.cam.ac.uk/',
	'rsslogourl' => $rssicon,
	'nonewstext' => 'This optional text is shown when there is no news to display',
	'seealsourl' => TRUE,
	'seealsodesc' => 'all Computing Service news',
	)
    );

// In a "real" web page, you'd need to handle errors differently - e.g. simply
// omit the news box from the page. You should not report the technical
// details of errors in web pages, except for convenience during private
// testing, since they'll be meaningless to users and may reveal information
// that could be of value to an attacker.

if ($result1 === FALSE)
{
    $result1 = "<p>sample news-box for the " . htmlentities($tag) . 
	" feed failed: " . $rss->last_error() . "</p>\n";
}
else
{
    $result1 = "<p><b>Using the " . htmlentities($tag) . 
	" news feed:</b></p>$result1\n";
}

// Retrieve and format data for the RSS feed identified by tag 
// 'www.admin-all'. 

$tag = 'www.admin-all';
$result2 = $rss->format_rssdata(
    array(
	'directory' => $directory,
	'tag' => $tag,
	'type' => 'v2+image',
	'heading' => 'University of Cambridge news',
	'headingurl' => 'http://www.admin.cam.ac.uk/news/',
// RSS feed logo+link is suppress if no URL supplied
//	'rsslogourl' => $rssicon,
	'seealsourl' => 'http://www.admin.cam.ac.uk/news/',
	'maxitems' => 3,
	    )
    );

// In a "real" web page, you'd need to handle errors differently - e.g. simply
// omit the news box from the page. You should not report the technical
// details of errors in web pages, except for convenience during private
// testing, since they'll be meaningless to users and may reveal information
// that could be of value to an attacker.

if ($result2 === FALSE)
{
    $result2 = "<p>sample news-box for the " . htmlentities($tag) . 
	" feed failed: " . $rss->last_error() . "</p>\n";
}
else
{
    $result2 = "<p><b>Using the " . htmlentities($tag) . 
	" news feed:</b></p>$result2\n";
}

// Retrieve and format data for the RSS feed identified by tag 
// 'ucsnews.csx-lookup', chosen because it's very often "empty" (no items) and
// would then show the nonewstext.

$tag = 'ucsnews.csx-lookup';
$rss = new Ucam_RSSformat();

$result3 = $rss->format_rssdata(
    array(
	'directory' => $directory,
	'tag' => $tag,
	'type' => 'v2',
	'heading' => 'lookup news',
	'headingurl' => 'http://ucsnews.csx.cam.ac.uk/',
	'rsslogourl' => $rssicon,
	'nonewstext' => 'This optional text is shown when there is no news to display',
	'seealsourl' => TRUE,
	'seealsodesc' => 'all Computing Service news',
	)
    );

// In a "real" web page, you'd need to handle errors differently - e.g. simply
// omit the news box from the page. You should not report the technical
// details of errors in web pages, except for convenience during private
// testing, since they'll be meaningless to users and may reveal information
// that could be of value to an attacker.

if ($result3 === FALSE)
{
    $result3 = "<p>sample news-box for the " . htmlentities($tag) .  
	" feed failed: " . $rss->last_error() . "</p>\n";
}
else
{
    $result3 = "<p><b>Using the " . htmlentities($tag) . 
	" news feed:</b></p>\n$result3\n";
}

?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- For use in real web pages, the CSS definitions should probably be in
    a separate file, referenced here by URL. For this example, the definitions
    are here for simplicity and clarity. 
-->
<title>RSS feed demo</title>
<style type="text/css">
.news_box a {text-decoration:none;}
.news_box a:link {color: #039;}
.news_box a:visited {color: #c33;}
.news_box a:link:hover, a:visited:hover {text-decoration: underline;}
.news_box a[href]:active {color: #c00; }
.news_box a img { border: none; }

/* for news box heading links */
.news_box h2 a {text-decoration:none;}
.news_box h2 a:link {color: #fff;}
.news_box h2 a:visited {color: #ff0;}
.news_box h2 a:link:hover, a:visited:hover {text-decoration: underline;}

.news_box {margin: 0 0 1em 1em; border: 1px #999 solid; padding: 0;}
.news_box h2 {font-weight: normal; font-size: 90%; margin: 0 0 0.5em 0;
	padding: 0.2em 0.2em 0.2em 1em; text-align:left; display: block; 
	color: #fff; background: #069; border:0;}
.news_box div.news_item h3 {margin:0; font-size: 90%; color: #036;} 
.news_box p {font-size:90%; margin: 1em; text-align: left;}
.news_box ul.news_item {margin:0 0.5em 0 1em; display:inline-block; 
	padding: 0.5em 0 0.2em  0;font-size: 80%;}
.news_box ul.news_item li { line-height:130%; list-style: none; 
	margin:0 1em 0 0; padding:0; display:inline-block; font-weight:normal;}
.news_box div.news_item ul.news_item li { line-height:130%; list-style:none;
	margin:0; padding:0; display:inline-block; font-weight:normal;}
.news_box ul.news_item li a {display:block; background: url("/images/more.gif")
	no-repeat left top; padding-left: 20px;min-height:18px;}
.news_box ul.news_item li a span.news_item_date {color: #000; 
	background: #ccc; font-size: 90%;}
.news_box .news_clear {clear: both;}
.news_item_title {font-weight: normal;}
.news_item_image {float:left; margin: 0 0.5em 0.5em 0; }
.news_item_image img { width: 72px; height: 72px; }
/* .newsbox p.news_seealso {text-align: right;} */
.news_box p.news_seealso {text-align: right;}
.news_box a img.news_rss_image {float:right ;margin: 0; padding: 0.1em;}
</style>
</head>

<body>
<?php echo $result1 ?>
<?php echo $result2 ?>
<?php echo $result3 ?>
<!-- Table used here only for simplicity and clarity. In general, CSS should be
     used for page layout, rather than tables. -->
<table width="100%">
<tr><td width="48%" valign="top"><?php echo $result1 ?></td><td width="4%">
 </td><td width="48%" valign="top"><?php echo $result2 ?></td></tr>
</table>
</body>
</html>

