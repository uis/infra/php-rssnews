<?php

// Tell PHP where to find the library classes. 
include_once '/site/admindir/lib/ucam_rssnews.php';

// Fetch the Computing Service's overall RSS feed from ucsnews.csx.cam.ac.uk.
Ucam_RSSfetch::get_feed(array(
	     'directory' => '/site/admindir/rss-data',
	     'url' => 'http://ucsnews.csx.cam.ac.uk/xml/rss20/feed.xml',
	     'tag' => 'ucsnews.csx-all',
	     )
    );

// Fetch the Computing Service's lookup RSS feed from ucsnews.csx.cam.ac.uk.
// It's almost always empty, hence useful for demonstrating how the news box
// appears with no items to display.
Ucam_RSSfetch::get_feed(array(
	     'directory' => '/site/admindir/rss-data',
	     'url' => 'http://ucsnews.csx.cam.ac.uk/xml/rss20/service/lookup/feed.xml',
	     'tag' => 'ucsnews.csx-lookup',
	     )
    );

// Fetch the University news and event feed from www.admin.cam.ac.uk.
Ucam_RSSfetch::get_feed(array(
	     'directory' => '/site/admindir/rss-data',
	     'url' => 'http://www.admin.cam.ac.uk/news/newsfeeds/all.xml',
	     'tag' => 'www.admin-all',
	     )
    );
exit;
?>
